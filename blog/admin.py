# -*- coding: utf-8 -*-

from django.contrib import admin
from blog.models import UserPost, Comment

admin.site.register(UserPost)
admin.site.register(Comment)