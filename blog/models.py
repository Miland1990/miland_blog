# -*- coding: utf-8 -*-

from django.db import models
import datetime


class UserPost(models.Model):
    title = models.CharField(u'Заголовок статьи', max_length=250)
    text = models.TextField(u'Текст статьи')
    date = models.DateTimeField(u'Дата публикации статьи', default=datetime.datetime.now())

    def __unicode__(self):
        return self.title


class Comment(models.Model):
    userpost = models.ForeignKey(UserPost)
    text = models.CharField(u'Текст комментария', max_length=200)
    date = models.DateTimeField(u'Дата создания комментария', auto_now_add=True)

    def __unicode__(self):
        return self.text
