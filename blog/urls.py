# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from blog.views import UserPostListView, UserPostDetailView, add_comment

urlpatterns = patterns('',
                       url(r'^$', UserPostListView.as_view(), name='list_user_posts'),
                       url(r'^(?P<pk>\d+)/$', UserPostDetailView.as_view(), name='detail'),
                       url(r'^(?P<userpost_id>\d+)/add_comment/$', add_comment, name='add_comment')
                       )
