# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse
from  django.template.context import RequestContext
from blog.models import UserPost
from django.views.generic import ListView, DetailView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import datetime
from blog.forms import CommentForm


class UserPostListView(ListView):
    model = UserPost
    template_name = 'blog/userpost_list.html'
    context_object_name = 'userposts'

    def get_queryset(self):
        title_presentation = UserPost.objects.filter(date__lte=datetime.datetime.now()).order_by('-date')
        paginator = Paginator(title_presentation, 3)
        page = self.request.GET.get('page')
        try:
            userpost = paginator.page(page)
        except PageNotAnInteger:
            userpost = paginator.page(1)
        except EmptyPage:
            userpost = paginator.page(paginator.num_pages)

        return userpost


class UserPostDetailView(DetailView):
    model = UserPost


def add_comment(request, userpost_id):
    userpost = UserPost.objects.get(pk=userpost_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.userpost_id = userpost_id
            new_comment.save()
            return HttpResponseRedirect(reverse('blog:detail', kwargs={'pk': userpost.id}))
    else:
        form = CommentForm()
    return render_to_response('blog/add_comment.html', {'form': form, 'userpost': userpost},
                              context_instance=RequestContext(request))
